package com.clkasd.backbaseassignment.Model;

/**
 * Created by Aykut on 9.12.2017.
 */

public class Coord {
    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    double lon;
    double lat;
}
