package com.clkasd.backbaseassignment.Model;

/**
 * Created by Aykut on 9.12.2017.
 */

public class City  {
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return _id;
    }


    public void setId(int id) {
        this._id = id;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    String country;
    String name;
    int _id;
    Coord coord;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        City city = (City) o;

        if (_id != city._id) return false;
        return name != null ? name.equals(city.name) : city.name == null;
    }
}
