package com.clkasd.backbaseassignment.Dashboard;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.clkasd.backbaseassignment.Fragments.MapFragment;
import com.clkasd.backbaseassignment.Model.City;
import com.clkasd.backbaseassignment.R;
import com.clkasd.backbaseassignment.Util.CitiesAdapter;

import java.util.List;

public class DashboardActivity extends AppCompatActivity implements IDashboardView ,CitiesAdapter.CityClickedListener{
    DashboardPresenter presenter;
    RecyclerView citiesView;
    ProgressBar progressIcon;
    CitiesAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }
    void initViews()
    {
        citiesView = findViewById(R.id.citiesView);
        progressIcon = findViewById(R.id.progressIcon);
        presenter = new DashboardPresenter(this);
        progressIcon.setVisibility(View.VISIBLE);
        presenter.init(this);
    }

    @Override
    public void showFilteredCities(List<City> cities,boolean isFirstTime) {
        if(isFirstTime) {
            isReadyToSearch = true;
            searchView.setVisibility(View.VISIBLE);
            adapter = new CitiesAdapter(cities,this);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            citiesView.setLayoutManager(layoutManager);
            citiesView.setItemAnimator(new DefaultItemAnimator());
            citiesView.setAdapter(adapter);
        }
        else {
            if(cities!=null && cities.size()>0) {
                findViewById(R.id.nocitiesfoundtext).setVisibility(View.GONE);
                citiesView.setVisibility(View.VISIBLE);
                adapter.setItemList(cities);
                adapter.notifyDataSetChanged();
            }
            else
            {
                findViewById(R.id.nocitiesfoundtext).setVisibility(View.VISIBLE);
                citiesView.setVisibility(View.GONE);
            }
        }
        progressIcon.setVisibility(View.GONE);
    }
    SearchView searchView;
    boolean isReadyToSearch = false;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        final SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(isReadyToSearch)
                    presenter.searchCities(newText);
                else {
                    Toast.makeText(DashboardActivity.this, R.string.warning_load,Toast.LENGTH_SHORT).show();
                    disableSearchView();
                }
                return true;
            }
        });
        return true;
    }

    @Override
    public void onCityClicked(City city) {
        MapFragment mapFragment = new MapFragment();
        Bundle bundle = new Bundle();
        bundle.putDouble("lat",city.getCoord().getLat());
        bundle.putDouble("lon",city.getCoord().getLon());
        bundle.putString("name",city.getName());
        mapFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack("map");
        transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out,android.R.animator.fade_in, android.R.animator.fade_out);
        transaction.replace(R.id.frame_layout, mapFragment);
        transaction.commit();
        disableSearchView();
    }

    private void disableSearchView() {
        searchView.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount()>0) {
            getSupportFragmentManager().popBackStack();
            searchView.setVisibility(View.VISIBLE);
        }
        else
            super.onBackPressed();
    }
}
