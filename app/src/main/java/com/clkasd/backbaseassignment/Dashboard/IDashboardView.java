package com.clkasd.backbaseassignment.Dashboard;

import com.clkasd.backbaseassignment.Model.City;

import java.util.List;

/**
 * Created by Aykut on 9.12.2017.
 */

public interface IDashboardView {
    void showFilteredCities(List<City> cities,boolean isFirstTime);
}
