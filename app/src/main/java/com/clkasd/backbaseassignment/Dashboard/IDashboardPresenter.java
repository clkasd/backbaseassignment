package com.clkasd.backbaseassignment.Dashboard;

import android.app.Activity;

/**
 * Created by Aykut on 9.12.2017.
 */

public interface IDashboardPresenter {
    void init(Activity activity);
}
