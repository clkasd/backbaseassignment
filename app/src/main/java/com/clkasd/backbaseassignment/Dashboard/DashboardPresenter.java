package com.clkasd.backbaseassignment.Dashboard;

import android.app.Activity;
import android.os.AsyncTask;

import com.clkasd.backbaseassignment.Model.City;
import com.clkasd.backbaseassignment.Util.CitiesFactory;
import com.clkasd.backbaseassignment.Util.Trie;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Aykut on 9.12.2017.
 */

public class DashboardPresenter implements IDashboardPresenter,CitiesFactory.JsonReadListener{
    IDashboardView view;
    List<City> cities;
    Trie prefixTree;
    public DashboardPresenter(IDashboardView view)
    {
        this.view = view;
    }


    @Override
    public void init(Activity activity) {
        CitiesFactory factory = new CitiesFactory();
        factory.setJsonReadListener(this);
        factory.getCities(activity);
    }
    void searchCities(String searchKey)
    {
        if(searchKey.equals(""))
            view.showFilteredCities(cities,false);
        else {
            List<City> results = prefixTree.getCitiesWithPrefix(searchKey);
            Collections.sort(results, alhpaComparator);
            view.showFilteredCities(results, false);
        }
    }

    Comparator<City> alhpaComparator = new Comparator<City>() {
        @Override
        public int compare(City city, City t1) {
            int compareResult=city.getName().compareTo(t1.getName());
            if(compareResult==0)
                return city.getCountry().compareTo(t1.getCountry());
            return compareResult;
        }
    };

    /**
     * First time file read and getting it ready to bind recycler view.
     * @param cities
     */
    @Override
    public void onFileRead(final List<City> cities) {
        new TrieInsertionTask().execute(cities);
    }
    private class TrieInsertionTask extends AsyncTask<List<City>,Void,Void>
    {

        @Override
        protected Void doInBackground(List<City>[] lists) {
            cities=lists[0];
            Collections.sort(cities,alhpaComparator);
            prefixTree = new Trie();
            for (City city : cities) {
                prefixTree.insert(city.getName(),city);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            view.showFilteredCities(cities,true);
        }
    }
}
