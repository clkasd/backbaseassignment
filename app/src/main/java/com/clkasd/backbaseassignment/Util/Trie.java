package com.clkasd.backbaseassignment.Util;

import com.clkasd.backbaseassignment.Model.City;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Aykut on 9.12.2017.
 */

/**
 * Trie data structure is basically a tree but inserting every char as a node into this tree.
 * This tree is also called prefix tree.
 * When you inserting a word into this tree create a node with every char from its start.
 * and when insertion is completed mark the last node as leaf.
 * for instance :
 *
 *          a
 *       /  |  \
 *      i   p   k
 *     /|
 *    m r
 *If you want to find "aim" you need to traverse tree to find first "a" char
 * and traverse its child nodes to find "i" and so on.
 * when you reached leaf node you found your search.
 *
 * Information about Trie data structure : https://en.wikipedia.org/wiki/Trie
 *
 */
public class Trie {
    private TrieNode root;

    public Trie()
    {
        root = new TrieNode();
    }
    /**
     * Trie insert function modified according to my needs. Every leaf node has to have
     * its cities connected to it.Improves data binding to recyclerview.
     * Also connectedcities is a list because there may be two cities with the same name.
     *
     *
     *Complexity : O(n*m) : n word lenght , m city count
     * @param word simply city name
     * @param city City object which is connected to city name
     */
    public void insert(String word, City city)
    {
        HashMap<Character, TrieNode> children = root.children;

        for(int i=0;i<word.length();i++)
        {
            char c = Character.toLowerCase(word.charAt(i));
            TrieNode t;
            if(children.containsKey(c))
            {
                t=children.get(c);
            }
            else
            {
                t = new TrieNode(c);
                children.put(c,t);
            }
            children = t.children;

            if(i==word.length()-1) {
                t.isLeaf = true;
                t.connectedCities.add(city);
            }
        }
    }

    /**
     * Complexity : O(n) n: string lenght
     * @param str
     * @return
     */
    private TrieNode searchNode(String str){
        Map<Character, TrieNode> children = root.children;
        TrieNode t = null;
        for(int i=0; i<str.length(); i++){
            char c = Character.toLowerCase(str.charAt(i));
            if(children.containsKey(Character.toLowerCase(c))){
                t = children.get(c);
                children = t.children;
            }else{
                return null;
            }
        }
        return t;
    }

    List<City> resultCities = new ArrayList<>();
    String cityName;
    String prefix;


    public List<City> getCitiesWithPrefix(String prefix) {
        resultCities.clear();
        this.prefix = prefix;
        cityName = prefix;
        TrieNode node = searchNode(prefix);
        if (node != null && node.children.size() > 0)
        {
            if (node.isLeaf)
                resultCities.addAll(node.connectedCities);
            applyDFS(node);
        }
        else if(node!=null && node.isLeaf && node.children.size()==0)
            resultCities.addAll(node.connectedCities);
        return resultCities;
    }
    /**
     *Complexity : O(n*d) n : children count d: depth
     *
     * To be able to get city names and its connected cities, applied DFS until we get to leaf.
     * Even if we reach to leaf there may be another city and another leaf beneath that node, so we must dig until it has no children.
     * @param node root node of the cityname which contains prefix
     */
    private void applyDFS(TrieNode node) {
        for (TrieNode trieNode :node.children.values()) {
            cityName += trieNode.c;
            if(trieNode.isLeaf)
            {
                resultCities.addAll(trieNode.connectedCities);
                if(trieNode.children.values().size()>0)
                    applyDFS(trieNode);
                else
                    cityName=prefix;
            }
            else
                applyDFS(trieNode);
        }
    }
}
