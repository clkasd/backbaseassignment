package com.clkasd.backbaseassignment.Util;

import com.clkasd.backbaseassignment.Model.City;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Aykut on 9.12.2017.
 */

public class TrieNode {
    char c;
    boolean isLeaf=false;
    List<City> connectedCities = new ArrayList<>();
    HashMap<Character, TrieNode> children = new HashMap<Character, TrieNode>();
    public TrieNode(){}
    public TrieNode(char c)
    {
        this.c=c;
    }
}
