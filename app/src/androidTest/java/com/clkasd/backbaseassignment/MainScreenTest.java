package com.clkasd.backbaseassignment;

import android.support.annotation.NonNull;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.clkasd.backbaseassignment.Dashboard.DashboardActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.internal.util.Checks.checkNotNull;

/**
 * Created by Aykut on 10.12.2017.
 */

@RunWith(AndroidJUnit4.class)
public class MainScreenTest {

    @Rule
    public ActivityTestRule<DashboardActivity> mActivityTule = new ActivityTestRule<DashboardActivity>(DashboardActivity.class);

    /**
     * Checking with valid city names
     */
    @Test
    public void checkAntalya() {
        onView(withId(R.id.search)).perform(typeText("antalya"),closeSoftKeyboard());
        onView(withId(R.id.citiesView)).check(matches(atPosition(0,hasDescendant(withText("Antalya,TR")))));
    }

    @Test
    public void checkIstanbul() {
        onView(withId(R.id.search)).perform(typeText("istanbul"),closeSoftKeyboard());
        onView(withId(R.id.citiesView)).check(matches(atPosition(0,hasDescendant(withText("Istanbul,TR")))));
    }

    /**
     * checking invalid city names
     */
    @Test
    public void checkInvalid() {
        onView(withId(R.id.search)).perform(typeText("@&&"),closeSoftKeyboard());
        onView(withId(R.id.nocitiesfoundtext)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }





    public static Matcher<View> atPosition(final int position, @NonNull final Matcher<View> itemMatcher) {
        checkNotNull(itemMatcher);
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("has item at position " + position + ": ");
                itemMatcher.describeTo(description);
            }

            @Override
            protected boolean matchesSafely(final RecyclerView view) {
                RecyclerView.ViewHolder viewHolder = view.findViewHolderForAdapterPosition(position);
                if (viewHolder == null) {
                    // has no item on such position
                    return false;
                }
                return itemMatcher.matches(viewHolder.itemView);
            }
        };
    }

}
